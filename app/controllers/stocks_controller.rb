class StocksController < ApplicationController
  def search
    if params[:stock].present?
      @stock = Stock.get_ticker_price(params[:stock])
      if @stock
        respond_to do |format|
          format.js { render partial: 'users/stock_data.js' }
        end
      else
        respond_to do |format|
          flash.now[:alert] = 'Please enter correct ticker symbol'
          format.js { render partial: 'users/stock_data.js' }
        end
      end
    else
      respond_to do |format|
        flash.now[:alert] = 'Please enter ticker symbol'
        format.js { render partial: 'users/stock_data.js' }
      end
    end
  end
end