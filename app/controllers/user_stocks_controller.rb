class UserStocksController < ApplicationController
  def create
    stock = Stock.is_stock_present?(params[:ticker])
    if stock.blank?
      stock = Stock.get_ticker_price(params[:ticker])
      stock.save
    end
    UserStock.create(user: current_user, stock: stock)
    flash[:notice] = 'Successfully added in portfolio'
    redirect_to portfolio_path
  end

  def destroy
    stock = Stock.find(params[:id])
    user_stock = UserStock.where(user_id: current_user.id, stock_id: stock.id).first
    user_stock.destroy
    # current_user.stocks.find(params[:id]).destroy
    flash[:notice] = "#{stock.ticker} was successfully removed"
    redirect_to portfolio_path
  end
end
