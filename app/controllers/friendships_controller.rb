class FriendshipsController < ApplicationController
  def create
    friend = User.find(params[:id])
    Friendship.create(user: current_user, friend: friend)
    flash[:notice] = "Successfully added in friend list"
    redirect_to friends_path
  end

  def destroy
    friend = User.find(params[:id])
    user_friend = Friendship.where(user_id: current_user.id, friend_id: friend.id).first
    user_friend.destroy
    flash[:notice] = "#{friend.full_name} successfully removed"
    redirect_to friends_path
  end
end