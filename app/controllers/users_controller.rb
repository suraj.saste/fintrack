class UsersController < ApplicationController
  def portfolio
    @user = current_user
    @tracked_stocks = current_user.stocks
  end

  def friends
    @friends = current_user.friends
  end

  def show
    @user = User.find(params[:id])
    @tracked_stocks = @user.stocks
  end

  def search
    if params[:friend].present?
      @friends = User.friend_search(params[:friend])
      @friends = current_user.except_current_user(@friends)
      unless @friends.empty?
        respond_to do |format|
          format.js { render partial: 'users/friend_data.js' }
        end
      else
        respond_to do |format|
          @friends = nil
          flash.now[:alert] = 'Please enter correct friend info'
          format.js { render partial: 'users/friend_data' }
        end
      end
    else
      respond_to do |format|
        flash.now[:alert] = 'Please enter friend info'
        format.js { render partial: 'users/friend_data.js' }
      end
    end
  end
end
