class Stock < ApplicationRecord
  has_many :user_stocks
  has_many :users, through: :user_stocks

  validates :name, :ticker, presence: true

  def self.get_ticker_price(ticker_symbol)
    client = IEX::Api::Client.new(publishable_token: Rails.application.credentials.iex_client[:sandbox_publishable_token],
                                  secret_token: Rails.application.credentials.iex_client[:sandbox_secret_token],
                                  endpoint: 'https://sandbox.iexapis.com/v1')
    begin
      new(name: client.company(ticker_symbol).company_name, ticker: ticker_symbol, price: client.price(ticker_symbol))
    rescue => exception
      return nil
    end
  end

  def self.is_stock_present?(ticker)
    find_by(ticker: ticker)
  end
end
