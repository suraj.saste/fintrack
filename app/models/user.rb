class User < ApplicationRecord
  has_many :user_stocks
  has_many :stocks, through: :user_stocks

  # self reference
  has_many :friendships
  has_many :friends, through: :friendships

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  def check_stock_in_portfolio(ticker)
    stocks.find_by(ticker: ticker).present?
  end

  def less_than_limit
    stocks.count < 10
  end

  def full_name
    return "#{first_name} #{last_name}" if first_name || last_name
    "-"
  end

  def self.matches(field, input)
    where("#{field} like ?", "%#{input}%")
  end

  def self.friend_search(input)
    input.strip!
    result = (matches("first_name", input) + matches("last_name", input) + matches("email", input)).uniq
    return nil unless result
    result
  end

  def except_current_user(friends)
    friends.reject {|friend| friend.id == self.id } 
  end

  def is_friend?(friend_id)
    friends.exists?(id: friend_id)
  end

  def stock_already_in_portfolio?(stock_id)
    stocks.exists?(id: stock_id)
  end
end
