class CreateUserStocks < ActiveRecord::Migration[6.0]
  def change
    create_table :user_stocks do |t|
      t.references :user, foreign_key: true, null: false
      t.references :stock, foreign_key: true, null: false

      t.timestamps
    end
  end
end
